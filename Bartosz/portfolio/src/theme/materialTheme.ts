import {
    createMuiTheme,
    responsiveFontSizes,
    Theme,
} from '@material-ui/core/styles';
import {
    ComponentNameToClassKey,
    Overrides,
    // eslint-disable-next-line import/no-unresolved
} from '@material-ui/core/styles/overrides';
import { StyleRules } from '@material-ui/core/styles/withStyles';

// Utility for merging two interfaces
type Modify<T, R> = Omit<T, keyof R> & R;

// Add here your custom components that you want to override in the theme
export type CustomNames = Modify<
    ComponentNameToClassKey,
    {
        Nav: string;
    }
    >;

// Overrides will work for custom components names
export type CustomOverrides = Modify<
    Overrides,
    { [Name in keyof CustomNames]?: Partial<StyleRules<CustomNames[Name]>> }
    >;

// Custom Theme should be used instead of theme
export type CustomThemeType = Modify<
    Theme,
    {
        overrides?: CustomOverrides;
    }
    >;

export const theme: CustomThemeType = createMuiTheme({
    palette: {
        primary: {
            main: '#0064F9',
            dark: '#1B42A8',
        },
        secondary: {
            main: '#878787',
            light: '#ebebeb',
            dark: '#505050',
        },
        success: {
            main: '#1CBC9D',
        },
        error: {
            main: '#E74C3C',
        },
        warning: {
            main: 'hsl(46, 100%, 60%)',
        },
        background: {
            default: '#f5f5f5',
        },
        text: {
            primary: '#15161A',
            secondary: '#fff',
        },
    },
    typography: {
        htmlFontSize: 10,
        h1: {
            fontSize: '4.5rem',
            fontWeight: 700,
            letterSpacing: '-1px',
        },
        h2: {
            fontSize: '3.5rem',
            fontWeight: 700,
            letterSpacing: '-1px',
        },
        h3: {
            fontSize: '3rem',
            fontWeight: 700,
        },
        h4: {
            fontSize: '2.5rem',
            fontWeight: 700,
        },
        body1: {
            fontSize: 16,
        },
    },
    breakpoints: {
        values: {
            xs: 414,
            sm: 768,
            md: 1024,
            lg: 1440,
            xl: 2048,
        },
    },
    spacing: 10,
});

theme.overrides = {
    MuiTypography: {
        body1: {
            fontSize: '1.7rem',
        },
        body2: {
            fontSize: '1.5rem',
        },
    },

    MuiFab: {
        root: {
            boxShadow: 'none',
        },
    },

    MuiButton: {
        root: {
            textTransform: 'none',
            fontWeight: 700,
        },
        label: {
            fontWeight: 900,
            fontSize: 17,
        },
        contained: {
            color: '#fff',
            border: '2px solid white',
            borderRadius: '3rem',

            paddingLeft: theme.spacing(5),
            paddingRight: theme.spacing(5),
            paddingTop: '1.4rem',
            paddingBottom: '1.8rem',
            boxShadow: 'none',
            '&:hover': {
                boxShadow: 'none',
            },
            '&:active': {
                boxShadow: 'none',
            },
        },
    },

    MuiAppBar: {
        root: {
            backgroundColor: theme?.palette?.text?.primary,
        },
    },

    MuiToolbar: {
        root: {
            maxHeight: '7.5rem',
        },
    },

    MuiTextField: {
        root: {},
    },

    MuiFormControl: {
        root: {
            padding: '0 1rem',
        },
    },

    MuiInputBase: {
        root: {
            padding: '0 1rem',
            borderRadius: '10px !important',
        },
        input: {
            paddingTop: '2rem',
            paddingBottom: '2rem',
            borderRadius: '8px',
        },
    },

    MuiFilledInput: {
        root: {
            '&.Mui-focused': {
                backgroundColor: undefined,
            },
        },
        input: {
            fontWeight: 900,
            paddingTop: '2rem',
            paddingBottom: '2rem',

            '&::placeholder': {
                fontWeight: 400,
            },
        },
        multiline: {
            padding: 0,
        },
        inputMultiline: {
            padding: '2rem',
        },
        error: {
            backgroundColor: '#fce4e4',
            border: '2px solid #fcc2c3',
            '&:hover, &:active, &:focus, &:focus-within': {
                backgroundColor: '#fce4e4',
            },
        },
    },

    MuiFormHelperText: {
        root: {
            color: theme.palette.error.main,
            fontWeight: 800,
            marginTop: theme.spacing(1),
            background: 'none',
            border: 'none',
            '&:hover': {
                background: 'none',
            },
        },
    },

    MuiFormControlLabel: { label: { fontWeight: 700 } },

    MuiSlider: {
        root: {
            color: theme.palette.primary.main,
            height: 8,
        },
        thumb: {
            height: 28,
            width: 28,
            backgroundColor: '#fff',
            boxShadow:
                '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)',
            marginTop: -10,
            marginLeft: -14,
            '&:focus, &:hover, &$active': {
                boxShadow:
                    '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
                // Reset on touch devices, it doesn't add specificity
                '@media (hover: none)': {
                    boxShadow:
                        '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)',
                },
            },
        },
        active: {},
        valueLabel: {
            width: 50,
            height: 50,
            left: 'calc(-50% + 12px)',
            top: -22,
            fontSize: '2rem',
            fontWeight: 700,
            '& *': {
                background: 'transparent',
                color: '#000',
                width: 70,
                height: 70,
            },
        },
        track: {
            height: 8,
            borderRadius: 4,
        },
        rail: {
            height: 8,
            borderRadius: 4,
        },
    },

    MuiInputAdornment: {
        root: {
            color: theme.palette.text.primary,

            '& *': {
                color: theme.palette.text.primary,
            },
        },
    },

    MuiChip: {
        label: {
            marginBottom: 2,
        },
    },

    MuiTab: {
        root: {
            textTransform: 'none',
            fontWeight: 700,
            paddingTop: theme.spacing(2),
            paddingBottom: theme.spacing(2),
            paddingLeft: 0,
            paddingRight: 0,
            minWidth: 0,
            '&:not(:last-of-type)': {
                marginRight: '4rem',
            },

            '& span': {
                width: 'auto',
            },
        },
    },

    MuiTabs: {
        indicator: {
            backgroundColor: 'white',
        },
    },

    // custom components
    Nav: {
        root: {
            backgroundColor: theme.palette.text.primary,
            position: 'relative',
            zIndex: 200,
        },
        link: {
            color: 'white',
            opacity: '0.35',
            textDecoration: 'none',
            fontWeight: 700,
            cursor: 'pointer',
        },
    },
    // Reset
    MuiCssBaseline: {
        '@global': {
            html: {
                fontSize: '62.5%',
            },

            body: {
                boxSizing: 'border-box',
                fontSize: '1.6rem',
            },

            svg: {
                fontSize: '1.7rem',
            },

            figure: {
                margin: 0,
            },
            a: {
                textDecoration: 'none',
                color: 'white',
                fontSize: 14,
            },
            ul: {
                padding: 0,
                margin: 0,
            },
        },
    },
};

export default responsiveFontSizes(theme);
